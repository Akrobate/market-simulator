/* istanbul ignore file */

'use strict';

const http_port = 3030;
const websocket_port = 3031;

const {
    HttpServerInstance,
} = require('./http_server/HttpServer');

const {
    WebsocketServerInstance,
} = require('./websocket_server/WebsocketServer');

HttpServerInstance.listen(http_port);
WebsocketServerInstance.listen(websocket_port);
