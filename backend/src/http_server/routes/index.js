'use strict';

const {
    Router,
} = require('express');

const api_routes = Router(); // eslint-disable-line new-cap

const url_prefix = '/api';

api_routes.get(
    '/markets',
    (request, response, next) => Promise
        .resolve({
            test: 'ok',
        })
        .catch(next)
);

module.exports = {
    api_routes,
    url_prefix,
};
