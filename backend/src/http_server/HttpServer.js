'use strict';

const {
    logger,
} = require('./../logger');

const cors = require('cors');
const express = require('express');

const routes_definitions = require('./routes');

const {
    error_manager_middleware,
    not_found_error_middleware,
} = require('./middlewares/HttpErrorManager');

class HttpServer {

    // eslint-disable-next-line require-jsdoc
    constructor(routes) {

        this.app = express();

        // Headers middleware
        this.app.use((request, response, next) => {
            if (request.headers) {
                response.header('Access-Control-Allow-Origin', '*');
                response.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization');
                response.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
                response.header('Access-Control-Allow-Credentials', 'false');
            }
            return next();
        });
        this.app.use(express.json());
        this.app.use(express.urlencoded({
            extended: false,
        }));

        this.app.use(cors({
            optionsSuccessStatus: 200, /* some legacy browsers (IE11, various SmartTVs) choke on 204 */
        }));

        this.app.use(routes.url_prefix, routes.api_routes);

        this.app.use(not_found_error_middleware);
        this.app.use(error_manager_middleware);
    }

    /**
     * @param {*} port
     * @return {Void}
     */
    listen(port) {
        this.app.listen(port, (error) => {
            if (error) {
                logger.log(error);
                process.exit(1); // eslint-disable-line no-process-exit
            }
            logger.log(`Http server simulator is listening on port ${port}`);
        });
    }


    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (!HttpServer.instance) {
            HttpServer.instance = new HttpServer(
                routes_definitions
            );
        }
        return HttpServer.instance;
    }

}


module.exports = {
    HttpServer,
    HttpServerInstance: HttpServer.getInstance(),
};
