'use strict';


class OhlcDataHolder {

    /**
     * @returns {Object}
     */
    static get JSON_FILE_COLUMNS() {
        return {
            OPEN_TIME: 0,
            OPEN: 1,
            HIGH: 2,
            LOW: 3,
            CLOSE: 4,
            VOLUME: 5,
            CLOSE_TIME: 6,
            QUOTE_ASSET_VOLUME: 7,
            NUMBER_OF_TRADES: 8,
            TAKER_BUY_BASE_ASSET_VOLUME: 9,
            TAKER_BUY_QUOTE_ASSET_VOLUME: 10,
        };
    }

    /**
     * @param {*} time
     * @param {*} open
     * @param {*} close
     * @param {*} high
     * @param {*} low
     * @param {*} volume
     * @returns {OhlcDataHolder}
     */
    constructor(time, open, close, high, low, volume) {
        this.time = Number(time);
        this.open = Number(open);
        this.close = Number(close);
        this.high = Number(high);
        this.low = Number(low);
        this.volume = Number(volume);
    }

    /**
     * @param {Obect} json_data
     * @returns {OhlcDataHolder}
     */
    static buildFromJsonData(json_data) {

        const time = json_data[OhlcDataHolder.JSON_FILE_COLUMNS.OPEN_TIME];
        const open = json_data[OhlcDataHolder.JSON_FILE_COLUMNS.OPEN];
        const high = json_data[OhlcDataHolder.JSON_FILE_COLUMNS.HIGH];
        const low = json_data[OhlcDataHolder.JSON_FILE_COLUMNS.LOW];
        const close = json_data[OhlcDataHolder.JSON_FILE_COLUMNS.CLOSE];
        const volume = json_data[OhlcDataHolder.JSON_FILE_COLUMNS.VOLUME];

        return new OhlcDataHolder(
            time,
            open,
            high,
            low,
            close,
            volume
        );
    }

    getTime() {
        return this.time;
    }

}

module.exports = {
    OhlcDataHolder,
};
