'use strict';

const {
    JsonFile,
} = require('../repositories/JsonFile');

const {
    OhlcDataHolder,
} = require('./OhlcDataHolder');

class Market {

    // eslint-disable-next-line require-jsdoc
    constructor(connection, json_file) {

        this.connection = connection;
        this.json_file = json_file;

        this.currency_pair = null;
        this.interval_value = null;
        this.interval_unit = null;

        this.ohlc_data = null;

        this.ohlc_current_step = 0;
        this.current_time = 0;
        this.status = Market.STATUS.IDLE;

        this.running_interval = null;
    }


    // eslint-disable-next-line require-jsdoc
    static get STATUS() {
        return {
            IDLE: 0,
            RUNNING: 1,
        };
    }

    /**
     * @param {WebSocketConnection} connection
     * @return {Market}
     */
    static build(connection) {
        return new Market(
            connection,
            JsonFile.buildInstance()
        );
    }

    /**
     * @return {void}
     */
    async run() {
        this.checkMarketReadyToUse();
        this.status = Market.STATUS.RUNNING;
        while (this.status === Market.STATUS.RUNNING) {
            await this.oneStepForward();
        }
    }


    /**
     * @return {void}
     */
    pause() {
        this.checkMarketReadyToUse();
        this.status = Market.STATUS.IDLE;
    }

    /**
     * @return {Promise<Void>}
     */
    async oneStepForward() {
        this.checkMarketReadyToUse();
        this.ohlc_current_step++;
        await this.process();
    }

    /**
     * @return {Void}
     */
    async oneStepBackward() {
        this.checkMarketReadyToUse();
        this.ohlc_current_step--;
        await this.process();
    }


    // eslint-disable-next-line require-jsdoc
    debug(data) {
        console.log(data);
    }

    /**
     * @return {Promise<void>}
     */
    async process() {
        console.log('Process Test');
    }

    // eslint-disable-next-line require-jsdoc
    init(currency_pair, interval_value, interval_unit) {
        this.currency_pair = currency_pair;
        this.interval_value = interval_value;
        this.interval_unit = interval_unit;
    }

    // eslint-disable-next-line require-jsdoc
    async loadDataset() {
        this.json_file
            .setFileName(`${this.currency_pair}_${this.interval_value}${this.interval_unit}.json`);
        const raw_data = await this.json_file.getData();
        this.ohlc_data = raw_data.map((item) => OhlcDataHolder.buildFromJsonData(item));
    }

    /**
     * @param {Integer} current_time timestamp in millis
     * @returns {void}
     */
    setCurrentTime(current_time) {
        const index = this.ohlc_data.findIndex((ohlc) => ohlc.getTime() >= current_time);
        this.ohlc_current_step = index;
        this.current_time = this.ohlc_data[index].getTime();
    }


    /**
     * Set the refresh rate of values when market simulation is running
     * @param {Integer} running_interval time in millis
     * @returns {void}
     */
    setRefreshInterval(running_interval) {
        this.running_interval = running_interval;
    }

    /**
     * @throws {Error}
     * @returns {Void}
     */
    checkMarketReadyToUse() {
        if (this.ohlc_data === null) {
            throw new Error('DATA WAS NOT LOADED');
        }
    }

}

module.exports = {
    Market,
};
