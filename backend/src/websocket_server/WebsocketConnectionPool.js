'use strict';

const {
    WebsocketConnection,
} = require('./WebsocketConnection');


class WebsocketConnectionPool {

    // eslint-disable-next-line require-jsdoc
    constructor() {
        this.connection_list = [];
    }

    // eslint-disable-next-line require-jsdoc
    declareConnection(connection_websocket_data) {
        this.addConnectionToPool(
            WebsocketConnection.buildConnection(connection_websocket_data)
        );
    }

    // eslint-disable-next-line require-jsdoc
    destroyConnection(connection) {
        const index = this.connection_list.findIndex((item) => item === connection);
        if (index > -1) {
            this.connection_list.splice(index, 1);
        }
    }

    // eslint-disable-next-line require-jsdoc
    addConnectionToPool(connection) {
        this.connection_list.push(connection);
    }


}


module.exports = {
    WebsocketConnectionPool,
};
