'use strict';

const {
    Market,
} = require('./../services/Market');

class WebsocketConnection {

    // eslint-disable-next-line require-jsdoc
    constructor(connection_websocket_data) {
        this.websocket_client = connection_websocket_data.websocket_client;
        this.connection_uuid = null;

        this.market = Market.build(this);
    }

    // eslint-disable-next-line require-jsdoc
    static buildConnection(connection_websocket_data) {
        return new WebsocketConnection(connection_websocket_data);
    }

    // eslint-disable-next-line require-jsdoc
    setConnectionUuid(uuid) {
        this.connection_uuid = uuid;
    }

    // eslint-disable-next-line require-jsdoc
    getConnectionUuid() {
        return this.connection_uuid;
    }

    // eslint-disable-next-line require-jsdoc
    notify(data) {
        console.log(data);
    }

}

module.exports = {
    WebsocketConnection,
};
