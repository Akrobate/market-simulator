'use strict';

const WebSocket = require('ws');

const {
    v4,
} = require('uuid');

const {
    logger,
} = require('../logger');

const {
    WebsocketConnectionPool,
} = require('./WebsocketConnectionPool');

class WebsocketServer {

    // eslint-disable-next-line require-jsdoc
    constructor(websocket_connection_pool) {
        this.websocket_connection_pool = websocket_connection_pool;
    }


    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (!WebsocketServer.instance) {
            WebsocketServer.instance = new WebsocketServer(
                new WebsocketConnectionPool()
            );
        }
        return WebsocketServer.instance;
    }

    // eslint-disable-next-line require-jsdoc
    listen(port) {
        logger.log(`Websocket server simulator is listening on port ${port}`);
        this
            .getWebSocketServer(port)
            .on('connection', (opened_web_socket) => {

                const connection = this
                    .websocket_connection_pool
                    .declareConnection({
                        websocket_client: opened_web_socket,
                    });

                opened_web_socket.on('message', (message) => {
                    const params = this.formatIncomingMessage(message);
                    connection.notify(params);
                });

                opened_web_socket.on('close', () => {
                    this.websocket_connection_pool
                        .destroyConnection(connection);
                });
            });
    }


    // eslint-disable-next-line require-jsdoc
    getWebSocketServer(port) {
        return new WebSocket.Server({
            port,
        });
    }


    /**
     * @param {String} message
     * @returns {Object}
     */
    formatIncomingMessage(message) {
        let data = null;
        try {
            data = JSON.parse(message);
            data.payload = data.payload ? data.payload : {};
        } catch (error) {
            data = {
                error,
                message,
            };
        }
        return data;
    }

    /**
     * @param {Object} data
     * @returns {String}
     */
    formatSendMessage(data) {
        return JSON.stringify(data);
    }

}

module.exports = {
    WebsocketServer,
    WebsocketServerInstance: WebsocketServer.getInstance(),
};
