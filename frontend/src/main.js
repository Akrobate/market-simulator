import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'

Vue.config.productionTip = false

Vue.use(Vuex)

import store from '@/store'

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
