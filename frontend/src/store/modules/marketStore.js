'use strict'

const state = () => ({
    data: [],
})

const getters = {
}

const actions = {
}

const mutations = {
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
