import Vue from 'vue'
import Vuex from 'vuex'
import marketStore from './modules/marketStore'
import appLayoutStore from './modules/appLayoutStore'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    market_store: marketStore,
    app_layout_store: appLayoutStore,
  },
});
