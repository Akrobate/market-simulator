


class WebsocketHandler {


    constructor() {
        this.connection = null;
        this.configuration = null;
    }

    setConfiguration(configuration) {
        this.configuration = configuration;
    }

    getConnection() {
        if (this.connection === null) {
            this.connection = this.createWebSocketConnection(this.configuration);
        }
        return this.connection;
    }

    createWebSocketConnection(configuration) {
        const connection = new WebSocket(configuration.websocket_server_url);
        try {
            if (connection.readyState === WebSocket.CONNECTING) {
                console.log('Waiting for connection to be etablished')
            } else {
                console.log('createWebSocketConnection connected')
            }
        } catch (error) {
          console.log('StorecreateWebSocketConnection', error)
          throw error;
        }
        return connection;
    }

}


export default {
    WebsocketHandler,
    websocket_handler: new WebsocketHandler(),
}
